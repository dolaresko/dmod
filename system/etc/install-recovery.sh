#!/system/bin/sh

BusyboxBin=/system/xbin/busybox
InstallRecoveryOrig=/system/etc/install-recovery-orig.sh

$BusyboxBin > /dev/null 2>&1 && $BusyboxBin grep -q run-parts /*.rc || /system/bin/logwrapper $BusyboxBin run-parts /system/etc/init.d

$BusyboxBin > /dev/null 2>&1 && $BusyboxBin test -x $InstallRecoveryOrig && $InstallRecoveryOrig
