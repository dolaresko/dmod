#!/sbin/sh
#Script to install-recovery.sh by Ryuinferno @ XDA 2012

busybox mount -o remount,rw -t auto /system

if [ -e /system/etc/install-recovery.sh ] 
then
busybox echo "# init.d support" >> /system/etc/install-recovery.sh
busybox echo "busybox run-parts /system/etc/init.d/" >> /system/etc/install-recovery.sh
busybox echo "" >> /system/etc/install-recovery.sh
busybox awk '!x[$0]++' /system/etc/install-recovery.sh > /tmp/install-recovery.sh
busybox cat /tmp/install-recovery.sh > /system/etc/install-recovery.sh
busybox echo "" >> /system/etc/install-recovery.sh
else
cat > /system/etc/install-recovery.sh
busybox echo "#!/system/bin/sh" >> /system/etc/install-recovery.sh
busybox echo "# init.d support" >> /system/etc/install-recovery.sh
busybox echo "busybox run-parts /system/etc/init.d/" >> /system/etc/install-recovery.sh
busybox echo "" >> /system/etc/install-recovery.sh
fi

if [ -e /system/bin/sysint ]
then
busybox echo "#!/system/bin/sh" >> /system/bin/sysint
busybox echo "# init.d support" >> /system/bin/sysint
busybox echo "" >> /system/bin/sysint
busybox echo "export PATH=/sbin:/system/sbin:/system/bin:/system/xbin" >> /system/bin/sysint
busybox echo "/system/bin/logwrapper run-parts /system/etc/init.d" >> /system/bin/sysint 
busybox echo "" >> /system/bin/sysint
busybox awk '!x[$0]++' /system/bin/sysint > /tmp/sysint
busybox cat /tmp/sysint > /system/bin/sysint
busybox echo "" >> /system/bin/sysint
else
busybox echo "#!/system/bin/sh" > /system/bin/sysint
busybox echo "# init.d support" >> /system/bin/sysint
busybox echo "" >> /system/bin/sysint
busybox echo "export PATH=/sbin:/system/sbin:/system/bin:/system/xbin" >> /system/bin/sysint
busybox echo "/system/bin/logwrapper run-parts /system/etc/init.d" >> /system/bin/sysint 
busybox echo "" >> /system/bin/sysint
fi

busybox chmod 755 /system/etc/install-recovery.sh
busybox chown 0.0 /system/etc/install-recovery.sh
busybox chmod 755 /system/bin/sysint
busybox chown 0.2000 /system/bin/sysint

